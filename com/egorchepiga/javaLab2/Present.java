package javaLab2;

public interface Present extends Product {
    Boolean itCanBePresented();
}
