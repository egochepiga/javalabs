package javaLab2;

public class Main {
    public static void main(String[] args)
    {
        Product[] productArray = { new Book(), new Shoe(), new Picture(), new Picture(), new Toy(), new Book()};
        for (int i = 0; i < productArray.length; i++)
            System.out.printf("%d. %s\n", i + 1, productArray[i].whoAmI());
        System.out.print("\n");
        for (int i = 0; i < productArray.length; i++)
            if (productArray[i] instanceof Present) {
                System.out.printf("%d. %s %b\n",
                    i, productArray[i].whoAmI(), ((Present)productArray[i]).itCanBePresented()
                );
            }
    }
}
