package javaLab2;

public class Picture implements Present {
    @Override
    public Boolean itCanBePresented() {
        return false;
    }

    @Override
    public String whoAmI() {
        return "Picture";
    }
}
