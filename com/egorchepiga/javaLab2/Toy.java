package javaLab2;

public class Toy implements Present {

    @Override
    public Boolean itCanBePresented() {
        return true;
    }

    @Override
    public String whoAmI() {
        return "Toy";
    }
}
