package javaLab1;
import java.lang.System;
import java.lang.Math;

public class Main {
    public static void main(String[] args) {
        int a, b, step;
        if (args.length < 3) {
            throw new RuntimeException("Ожидаем 3 аргумента");
        } else {
            try {
                a = Integer.parseInt(args[0]);
                b = Integer.parseInt(args[1]);
                step = Integer.parseInt(args[2]);
                if (a > b) {
                    throw new RuntimeException("Ожидаем, что a < b");
                }
            } catch (NumberFormatException e) {
                throw new RuntimeException("Ожидаем целочисленные аргументы");
            }
        }
        System.out.print("x     f(x)\n");
        for (int i = a; i < b + 1; i += step) {
            System.out.printf("%d     %f\n", i, Math.tan(2 * i) - 3);
        }
    }
}
