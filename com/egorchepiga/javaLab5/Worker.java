package javaLab5;

public class Worker  extends Thread {
    private TeamLeader manager;
    private int number = -1;
    private String task = "";
    private int sizeOfTrees = 0;

    @Override
    public void run() {
        for (int i = 0; i < this.sizeOfTrees; i++) {
            while(manager.x != this.number) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            manager.change();
            System.out.printf("Роботник № %d %s для дерева №%d\n", this.number + 1, this.task, i + 1);
        }
    }

    void setTasks(String task) {
        this.task = task;
    }
    void setNumber(int number) { this.number = number; }
    void setSizeOfTrees(int sizeOfTrees) {
        this.sizeOfTrees = sizeOfTrees;
    }
    void setManager(TeamLeader manager) {
        this.manager = manager;
    }
}
