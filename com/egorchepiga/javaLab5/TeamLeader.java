package javaLab5;

public class TeamLeader {
    int x = 0;

    synchronized void change()
    {
        this.x++;
        if (this.x > 2) {
            this.x = 0;
        }
        try{
            Thread.sleep(100);
        }
        catch(InterruptedException e){}
    }
}