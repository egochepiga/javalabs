package javaLab5;

public class Main {
    public static void main(String[] argv) {
        TeamLeader teamLeader = new TeamLeader();
        int sizeOfTrees ;
        int WORKER_SIZE = 3;
        String[] tasks = { "копает яму", "сажает дерево", "подвязывает саженец к кольям" };
        if (argv.length != 1) throw new RuntimeException("Ожидаем входных аргументов");
        try {
            sizeOfTrees = Integer.parseInt(argv[0]);
            if (sizeOfTrees <= 0) throw new RuntimeException("Аргумент должен быть положительным, целым числом");
        } catch (NumberFormatException e) {
            throw new RuntimeException("Аргумент должен быть положительным, целым числом");
        }
        Worker[] robots = new Worker[WORKER_SIZE];
        for (int i = 0; i < WORKER_SIZE; i++) {
            robots[i] = new Worker();
            robots[i].setTasks(tasks[i % (WORKER_SIZE)]);
            robots[i].setSizeOfTrees(sizeOfTrees);
            robots[i].setManager(teamLeader);
            robots[i].setNumber(i);
        }
        for (Worker robot : robots) robot.start();
    }
}
