package javaLab3;

import java.io.*;
import java.util.Properties;

public class Main {
    public static void main(String[] args)
    {
        FileInputStream outputStream = null;
        Properties properties = new Properties();
        try {
            outputStream = new FileInputStream("config.properties");
        } catch (FileNotFoundException e) {
            System.out.print("Файл не найден");
            return;
        }
        try {
            properties.load(outputStream);
        } catch (IOException e) {
            System.out.print("Невозможно открыть properties");
            return;
        }
        try {
            InputStreamReader inputStreamReadernew = new InputStreamReader(System.in);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReadernew);
            while (true) {
                System.out.print("Введите property ключ: ");
                String input = bufferedReader.readLine();
                if (input == null || input.length() == 0) {
                    break;
                }
                if (properties.containsKey(input)) {
                    System.out.printf("%s  %s\n", input, properties.getProperty(input));
                } else {
                    System.out.printf("Ключ не найден %s \n", input);
                }
            }
            bufferedReader.close();

        } catch (IOException e) {
            System.out.print("Невозможно прочитать properties");
        }
    }
}
