package javaLab4;

import java.io.*;
import java.util.Properties;
import java.util.HashMap;
import javaLab4.MyPropertyreader;

public class Main {
    public static void main(String[] args)
    {
        MyPropertyreader mpr = new MyPropertyreader("config.properties");
        HashMap<String,String> props = mpr.load();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                System.out.print("Введите property ключ...");
                String input = bufferedReader.readLine();
                if (input == null || input.length() == 0) {
                    break;
                }
                if (props.containsKey(input)) {
                    System.out.printf("%s  %s\n", input, props.get(input));
                } else {
                    System.out.printf("Ключ не найден %s \n", input);
                }
            }
            bufferedReader.close();
        } catch (IOException e) {
            System.out.print("Невозможно прочитать property.");
        }
    }
}
